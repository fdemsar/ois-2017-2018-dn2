/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://teaching.lavbic.net/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  };
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://teaching.lavbic.net/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://teaching.lavbic.net/OIS/gradivo/") > -1;
   var jeVideo = sporocilo.indexOf("youtube.com") > -1;
  if (jeSmesko) {
    sporocilo =
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } 
  if(jeVideo){
    return divElementHtmlTekst2(sporocilo);
  }
  else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}
function divElementHtmlTekst2(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */






var omenjeni=[];

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
   var tabYT=[];
   var tabYTid=[]; //to so le konci od linkov
   var video="";

  
  var tekst=sporocilo.split(' ');
  var indeks=0;
  
  for(var k=0; k<tekst.length; k++){
    if(tekst[k].substring(0, 32) == "https://www.youtube.com/watch?v="){
      console.log("jeVideo");
      console.log(tekst[k]);
      tabYT[indeks]=tekst[k];
      tabYTid[indeks]=tekst[k].substring(32, tekst[k].length);
      console.log("v tabeli je link z ID"+tabYTid[k]);
      indeks++;
    }
  }
  
   for(var i=0; i<tabYTid.length; i++){
        var link="https://www.youtube.com/embed/" + tabYTid[i];
        console.log(link);
        if(i==0){
          video += "<br><iframe width='200px' height='150px' style = 'margin-top:10px; margin-left:10px'  src='https://www.youtube.com/embed/" + tabYTid[i] + " ' allowfullscreen></iframe>";
        }else{
          video += "<iframe width='200px' height='150px' style = 'margin-top:10px; margin-left:10px'  src='https://www.youtube.com/embed/" + tabYTid[i] + " ' allowfullscreen></iframe>";
        }     
        console.log(video);
    }
   //praznjenje tabele
    tabYTid=[];
    tabYT=[];
    sporocilo=sporocilo+video;
  
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtrirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }
 
 video="";
  $('#poslji-sporocilo').val('');
}


// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('./swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n');
});


/**
 * Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj 
 * z enako dolžino zvezdic (*)
 * 
 * @param vhodni niz
 */
function filtrirajVulgarneBesede(vhod) {
  for (var i in vulgarneBesede) {
    var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
    vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
  }
  return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";


// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  
  var stevec=0;
  var besedeSporocila =sporocilo.besedilo.split(' ');
 
  for (var i = 0; i < besedeSporocila.length; i++) {
    if (besedeSporocila[i].charAt(0) == '@') {
        omenjeni[stevec] = besedeSporocila[i].substring(1, (besedeSporocila[i].length+1));
        console.log(omenjeni);
        console.log(besedeSporocila[i].substring(1, (besedeSporocila[i].length+1)));
        stevec++;
    }
  }
  
  var vir = besedeSporocila[0].substring(0,(besedeSporocila[0].length-1));
  console.log(vir);
    

  for(var j=0; j<omenjeni.length; j++){
      //kako naj tu dolocim da gre le tisitm, ki so v tabeli omenjeni[j]
   if(omenjeni[j]==trenutniVzdevek && omenjeni[j]!=vir){ 
      var tistiKiTaga=sporocilo.besedilo.split(':');
      sporocilo=tistiKiTaga[0]+" (zasebno): &#9758; Omemba v klepetu";
      $('#sporocila').append(divElementHtmlTekst(sporocilo));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    }
  }
  omenjeni=[];

     $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));

    

  });
  
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabniki', function(uporabniki) {
    $("#seznam-uporabnikov").empty();
    for (var i=0; i < uporabniki.length; i++) {
      $("#seznam-uporabnikov").append(divElementEnostavniTekst(uporabniki[i]));
    }
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});


/* global $, io, Klepet */